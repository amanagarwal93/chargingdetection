package com.aman.chargingdetection

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    var isDetectionStarted: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)

        startDetection.setOnClickListener {
            isDetectionStarted = true
            registerReceiver(broadcastReceiver, intentFilter)
        }

        stopDetection.setOnClickListener {
            isDetectionStarted = false
            unregisterReceiver(broadcastReceiver)
        }
    }

    override fun onResume() {
        super.onResume()
        main()
    }

    fun main() {
        val timer = Timer()

        val calendar = Calendar.getInstance().time
        val task = object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    Toast.makeText(this@MainActivity, "" + calendar, Toast.LENGTH_SHORT).show()
                }
            }
        }
        timer.schedule(task, 0, 1000 * 60 * 5)
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            updateBatteryData(context, intent)
        }
    }

    private fun updateBatteryData(context: Context?, intent: Intent?) {

        val status = intent?.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
        val batteryLevel = intent?.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)

        val isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING

        when {
            isCharging -> {
                isDetectionStarted = false
                val chargePlug = intent!!.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)

                val usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB
                val acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC

                when {
                    usbCharge -> {
                        Toast.makeText(
                            context,
                            "Charging with USB\n Battery Level = " + batteryLevel + " % ",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    acCharge -> {
                        Toast.makeText(
                            context,
                            "Charging with AC\n Battery Level = " + batteryLevel + " % ",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            else -> when {
                !isDetectionStarted -> {
                    Toast.makeText(
                        context,
                        "Discharging Started\n Battery Level = " + batteryLevel + " % ",
                        Toast.LENGTH_SHORT
                    ).show()
                    isDetectionStarted = true
                }
            }
        }
    }
}
